<?php

use App\Http\Controllers\ExerciceController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/exercices', [ExerciceController::class, 'index'])->name('exercices.index');
Route::get('/exercices/create', [ExerciceController::class, 'create'])->name('exercices.ajouter');
Route::post('/exercices', [ExerciceController::class, 'store'])->name('exercices.store');
