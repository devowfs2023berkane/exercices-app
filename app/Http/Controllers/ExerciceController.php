<?php

namespace App\Http\Controllers;

use App\Models\Exercice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ExerciceController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        // consultations de la table exercices
        $exercices = Exercice::all();
        return view('exercices.index', compact('exercices'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        // formulaire de création d'un exercices
        return view('exercices.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        // enregistrement d'un exercices
        // 1er méthode classique POO
//        $exercice = new Exercice();
//        $exercice->titre = $request->titre;
//        $exercice->enonce = $request->enonce;
//        $exercice->user_id = Auth::user()->id;
//        $exercice->save();
        // 2ème méthode avec la méthode create eloquent
        $exercice = Exercice::create([
            'titre' => $request->titre,
            'enonce' => $request->enonce,
            'user_id' =>  Auth::user()->id ?? 1
        ]);
        return redirect()->route('exercices.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        // affichage d'un exercices
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        // formulaire d'édition d'un exercices
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        // mise à jour d'un exercices
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        // suppression d'un exercices
    }
}
