@extends('layouts.main')

@section('content')
    <h1>Liste d'exercices</h1>
    <button class="btn btn-success"><a href="{{route('exercices.ajouter')}}">Ajouter</a></button>
    @forelse($exercices as $exerice)
        <div class="card" style="width: 18rem;">
            <div class="card-body">
                <h5 class="card-title">{{$exerice->titre}}</h5>
                <h6 class="card-subtitle mb-2 text-body-secondary">{{ $exerice->created_at }}|
                    Par: {{ $exerice->user->name}}</h6>
                <p class="card-text">{{$exerice->enonce}}</p>
                <a href="#" class="card-link">Editer</a>
                <a href="#" class="card-link">Supprimer</a>
            </div>
        </div>
    @empty
        <p>Pas d'Exercices</p>
    @endforelse
@endsection
