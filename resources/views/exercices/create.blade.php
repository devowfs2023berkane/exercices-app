
@extends('layouts.main')

@section('content')
    <h1>Créer un exercice</h1>
    <form action="{{ route('exercices.store') }}" method="post">
        @csrf
        <div class="mb-3">
            <label for="titre" class="form-label">Titre</label>
            <input type="text" class="form-control" id="titre" name="titre">
        </div>
        <div class="mb-3">
            <label for="enonce" class="form-label">Enoncee</label>
            <input type="text" class="form-control" id="enonce" name="enonce">
        </div>
        <button type="submit" class="btn btn-primary">Créer</button>
    </form>
@endsection
